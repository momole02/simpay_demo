<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class SimpayHelper{

    private static $endpoint = "https://simpay.tech";
    private static $key = "26decc6dd109c5bc49d6a3bb8c9ff89678d5a94c208bd92c1cbea604dd2da46a";

    /**
     * Initiate a payment
     */
    public static function initiatePayment($amount, $product ,  $number, $expiration_date ){


        $client = new Client([
            'base_uri' => self::$endpoint,
        ]);

        $response = $client->request('POST' , '/api/v1/payment/initiate?_key='.self::$key , [
            'json' =>[
                'amount' => $amount ,
                'product' => $product ,
                'number' => str_replace(" " , "" , $number), //remove spaces
                'expiration_date' => $expiration_date
            ]
        ]);
        if($response->getStatusCode() == 200){
            $response_data = json_decode($response->getBody()->getContents(), TRUE);
            return $response_data['data']['payment_code'];
        }
        return null ;
    }

    /**
     * Get payment details
     */
    public static function getPayment($code){
        $client = new Client([
            'base_uri' => self::$endpoint
        ]);

        $response = $client->request('POST' , '/api/v1/payment/details/'.$code.'?_key='.self::$key);
        if($response->getStatusCode() == 200){
            $response_data = json_decode($response->getBody()->getContents() , TRUE);
            return $response_data['data'];
        }
        return null ;
    }
}
