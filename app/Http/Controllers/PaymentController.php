<?php

namespace App\Http\Controllers;

use App\Helpers\SimpayHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    /**
     * Show the payment form
     */
    public function paymentForm(){
        $products = DB::table('panier')->get();
        $operators = DB::table('operateur')->get();

        return view('payment_form')
        ->with("products" , $products)
        ->with('operators' , $operators);
    }

    public function doInitiatePayment(Request $req){
        $req->validate([
            'name' => 'required' ,
            'operator'=>'required' ,
            'drop-number' => 'required' ,
            'prod-total' => 'required']
        );

        $name = $req->post('name');
        $operator = $req->post('operator');
        $dropNumber = $req->post('drop-number');
        $amount = $req->post('prod-total');

        $expirationDate = date("Y-m-d H:i:s" , strtotime("+30 minutes"));
        try{
            $paymentCode = SimpayHelper::initiatePayment($amount , "TEST-PRODUCT" , $dropNumber , $expirationDate);
            DB::table('paiement')->insert([
                'code_pai' => $paymentCode ,
                'nom_pai' => $name ,
                'montant_pai' => $amount ,
                'paye_pai' => 0 ,
                'num_dep_pai' => $dropNumber ,
                'id_op' => $operator,
                'expire_date_pai' => $expirationDate
            ]);
            return redirect()->route('payment.info' , ['paycode' => $paymentCode]);
        }catch(\Exception $e){
            return back()->with('error-message' , "Erreur lors de l'initiation du paiement : ".$e->getMessage());
        }
    }

    public function paymentInfo($payCode){
        $payment = DB::table('paiement')
            ->where('code_pai' , $payCode)
            ->join('operateur', 'operateur.id_op' , '=' , 'paiement.id_op')
            ->first();

        if(null !== $payment){
            return view('payment_info' , ['payment' => $payment]);
        }else{
            abort(404);
        }
    }

    public function paymentUpdate($payCode){
        $payment = DB::table('paiement')
            ->where('code_pai' , $payCode)
            ->join('operateur', 'operateur.id_op' , '=' , 'paiement.id_op')
            ->first();
        if( null != $payment ){
            $simpayPaymentData = SimpayHelper::getPayment($payCode);
            $ratio = (float) $simpayPaymentData['filled_ratio'];
            DB::table('paiement')->update([
                'paye_pai' => $ratio * $payment->montant_pai
            ]);
            return response()->json([
                'amount' => $payment->montant_pai ,
                'paid' => $ratio * $payment->montant_pai,
                'ratio' =>  $ratio
            ]);
        }
        return response(null , 404);
    }

    public function paymentHistory(){
        $payments = DB::table('paiement')->get();
        return view('payment_history')->with('payments' , $payments);
    }
}
