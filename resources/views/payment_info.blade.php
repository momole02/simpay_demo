@extends('layout')

@section('title')
Paiement en cours ...
@stop

@section('pageTitle')
Paiement en cours ...
@stop


@section('pageContent')
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">Indication sur le paiement</div>
            <div class="card-body">
                Effectuez votre dépot sur le <br><br>
                <h3 class="text-center">
                {{$payment->num_depot_op}}
                </h3>
                <br>
                <div class="alert alert-danger">
                    <b>Verifiez que vous effectuez votre paiement à partir du numéro suivant : <br>
                        <div class="text-center">
                            {{ $payment->num_dep_pai }}
                        </div>
                    </b>
                </div>
                <div class="alert alert-warning">
                    <ul>
                        <li>Tout dépôt effectué à <b>partir</b> d'un autre numéro que celui indiqué sera considéré comme perdu</li>
                        <li>Tout dépôt effectué  <b>sur</b> un autre numéro que celui indiqué sera considéré comme perdu</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">Progression du paiement</div>
            <div class="card-body">
                <div class="alert alert-info" style="font-size: 13px">
                    Code de paiement simpay: <span id="paycode">{{$payment->code_pai}}</span>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>Total à payer</th>
                        <td> <span class="text-danger">{{$payment->montant_pai}} XOF</span> </td>
                    </tr>
                    <tr>
                        <th>Montant payé</th>
                        <td ><span id="paymentPaid">{{$payment->paye_pai}}</span> XOF</td>
                    </tr>
                    <tr>
                        <th>Montant restant</th>
                        <td><span id="paymentRemaining">{{$payment->montant_pai - $payment->paye_pai }}</span> XOF</td>
                    </tr>
                </table>
                Progression du paiement :
                @php($pct = 100 * (float)$payment->paye_pai/$payment->montant_pai )
                <div class="progress">
                <div id="paymentProgress" class="progress-bar" role="progressbar" style="width: {{$pct}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">{{$pct}}%</div>
                </div>
                <br>
                <button class="btn btn-primary" id="refreshButton">Rafraichir</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('extra_js')
<script type="text/javascript">
    function refresh(){
        console.log("Refreshing...");
        let code = $("#paycode").html();
        $.ajax({
            url : "/payment/update/"+code ,
            method : "GET" ,
            success : function (data,status,xhr){
                let pct = data.ratio * 100;
                $("#paymentProgress").html(pct+'%');
                $("#paymentProgress").css('width' , pct+'%');
                $("#paymentProgress").attr('aria-valuenow' , pct);
                $("#paymentPaid").html(data.paid);
                $("#paymentRemaining").html(data.amount - data.paid);
            },
            error: function(xhr , status , error){
                console.log(error);
            }
        });
    }

    $("#refreshButton").on('click' , refresh);
    setInterval(refresh , 3000);

</script>
@stop
