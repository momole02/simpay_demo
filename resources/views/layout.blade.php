<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield("title" , "Default title")</title>


  <!-- Bootstrap core CSS -->
  <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
  @yield('extra_css')
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-info static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Simpay Demo</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
          <a class="nav-link" href="{{route('payment.form')}}">Paiement
            </a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="{{route('payment.history')}}">Historique des transactions</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="mt-5">@yield("pageTitle" , "Default page title")</h1>
        <hr>
      </div>
    </div>

    @if(Session::has('error-message'))
    <div class="row">
        <div class="col-lg-12">
        <div class="alert alert-danger">{{Session::get('error-message')}}</div>
        </div>
      </div>
    @endif


    @if(Session::has('success-message'))
    <div class="row">
        <div class="col-lg-12">
        <div class="alert alert-success">{{Session::get('success-message')}}</div>
        </div>
      </div>
    @endif


    @yield("pageContent" , "Content")

  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('js/jquery.js')}}"></script>
  {{-- <script src="{{ asset('js/jquery.slim.min.js')}}"></script> --}}
  <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
  @yield('extra_js')
</body>

</html>
