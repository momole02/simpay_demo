@extends('layout')

@section('title')
Historique de paiement
@stop

@section('pageTitle')
Historique de paiements
@stop

@section('pageContent')
<table class="table" id="dataTable">
<thead>
    <tr>
        <th>Code de paiement</th>
        <th>Initié par</th>
        <th>Montant </th>
        <th>Tot payé </th>
        <th>Tot restant </th>
        <th>Date d'expiration</th>
        <th>Voir page de paiement</th>
    </tr>
</thead>
<tbody>
    @foreach($payments as $pay)
    <tr>
        <td>{{$pay->code_pai}}</td>
        <td>{{$pay->nom_pai}}</td>
        <td>{{$pay->montant_pai}}</td>
        <td>{{$pay->paye_pai}}</td>
        <td>{{$pay->montant_pai - $pay->paye_pai}}</td>
        <td>{{$pay->expire_date_pai}}</td>
    <td><a href="{{route('payment.info' , ['paycode' => $pay->code_pai])}}" class="btn btn-info">Voir</a></td>
    </tr>
    @endforeach
</tbody>
</table>
@stop


@section('extra_css')
<link rel="stylesheet" href="{{asset('vendor/datatables/datatables.min.css')}}">
@stop


@section('extra_js')
<script type="text/javascript" src="{{asset('vendor/datatables/datatables.min.js')}}"></script>
<script text="text/javascript">
    $("#dataTable").DataTable();
</script>
@stop
