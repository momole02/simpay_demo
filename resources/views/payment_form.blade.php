@extends('layout')

@section('title')
Finalisez votre paiement
@stop

@section('pageTitle')
Finalisez votre paiement
@stop

@section('pageContent')


@php($total = 0)
@foreach($products as $prod)
@php($total += $prod->prix_pan )
@endforeach

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">Informations de paiement</div>
            <div class="card-body">
            <form method="post" action="{{route('payment.initiate.do')}}" >
                    @csrf
                    <div class="form-group">
                        <label>Nom & prénoms : </label>
                        <input type="text" class="form-control" name="name">
                    </div>

                    <div class="form-group">
                        <label>Opérateur téléphonique : </label>
                        <select name="operator" class="form-control">
                            @foreach($operators as $op)
                                <option value="{{$op->id_op}}">{{$op->nom_op}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Numéro de téléphone de dépot</label>
                        <input type="text" class="form-control" name="drop-number">
                    </div>
                    <div class="alert alert-warning"><b>(!)</b> Faites attention d'entrer exactement votre <b>propre</b> numéro de téléphone, sinon vos dépôts ne seront pas pris en compte et votre argent sera perdu. </div>
                    <input type="hidden" name="prod-total" value="{{$total}}">

                    <button class="btn btn-primary" type="submit">Valider</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">Panier</div>
            <div class="card-body">

            <table class="table">
                <tr>
                    <th>Article</th>
                    <th>Prix</th>
                </tr>
                @foreach($products as $prod)
                <tr>
                <td>{{$prod->article_pan}} </td>
                <td>{{$prod->prix_pan}} XOF</td>
                </tr>
                @endforeach
            </table>
            <b><u>Total à payer</u></b> <br>
            <b class="text-danger" style="font-size : 18px"> {{$total}} XOF</b>
            </div>
        </div>
    </div>
</div>
@endsection
