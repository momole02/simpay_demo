<?php

use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PaymentController::class, "paymentForm"])->name('payment.form');
Route::get('/payment/{paycode}' , [PaymentController::class , "paymentInfo"])->name("payment.info");
Route::get('/history' , [PaymentController::class , "paymentHistory"])->name("payment.history");

Route::post('/payment/initiate.do' , [PaymentController::class , "doInitiatePayment"])->name('payment.initiate.do');
Route::get('/payment/update/{paycode}' , [PaymentController::class , "paymentUpdate"]);
